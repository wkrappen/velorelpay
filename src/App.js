import "./App.scss";
import { Canvas, useFrame } from "@react-three/fiber";
import ColorHash from "color-hash";
import {
  Bounds,
  OrthographicCamera,
  useBounds,
  Html,
  Trail,
} from "@react-three/drei";
import { Suspense, useMemo, useRef, useEffect } from "react";
import { suspend } from "suspend-react";
import create from "zustand";
import { useParams } from "react-router-dom";
import * as THREE from "three";
const colorHash = new ColorHash();
const useStore = create((set) => ({
  clock: new THREE.Clock(false),
  selected: [],
  setSelected: (selected) => set({ selected }),
  results: {},
  setResults: (results) => set({ results }),
  bounds: {
    minX: -200,
    maxX: 200,
    minY: -200,
    maxY: 200,
    minZ: -200,
    maxZ: 200,
  },
  setBounds: (bounds) => set({ bounds }),
}));

function App() {
  return (
    <>
      <Suspense fallback={null}>
        <RaceData></RaceData>
      </Suspense>
      <Canvas linear flat>
        <directionalLight
          position={[5, -10, 5]}
          intensity={2}
        ></directionalLight>
        <ambientLight></ambientLight>
        <Scene></Scene>
      </Canvas>
    </>
  );
}

function RaceData() {
  let { id } = useParams();
  let { selected, setSelected, setResults, clock, results } = useStore();
  const data = suspend(async () => {
    const res = await fetch(`https://fastboiii.sigsegowl.xyz/racedata/${id}`);
    return res.json();
  }, [id]);
  let track = useMemo(() => {
    let obj = {};
    data.results.forEach((r) => {
      obj[r.id] = r;
    });
    setResults(obj);
    return makeRacers(data);
  }, [data, setResults]);
  useEffect(() => {
    if (results) {
      console.log(track);
      let s = track.racers
        .filter(r=>r.results[0].file)
        .map((r) => r.results[0].id);
      setSelected(s);
    }
  }, [results, track,setSelected]);
  return (
    <div className="raceData">
      {track.name}
      <button
        onClick={() => {
          clock.start();
        }}
      >
        (Re)Start
      </button>
      <div className="racers">
        {track.racers.map((r) => {
          return (
            <div className="racer">
              <div className="name">{r.name}</div>
              <div className="times">
                {r.results.map((res) => {
                  return (
                    <div
                      onClick={() => {
                        if (!res.file) {
                          return;
                        }
                        if (selected.includes(res.id)) {
                          setSelected(selected.filter((s) => s !== res.id));
                        } else {
                          setSelected([...selected, res.id]);
                        }
                      }}
                      className={
                        "res " +
                        (selected.includes(res.id) ? "selected " : " ") +
                        (res.file ? "hasFile " : " ")
                      }
                    >
                      <div className="time">{res.time}</div>
                    </div>
                  );
                })}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
function Scene() {
  const camRef = useRef();
  //useHelper(camRef, CameraHelper, "red");
  let { selected, results } = useStore();
  //bounds to center and size:

  return (
    <>
      <OrthographicCamera
        makeDefault
        rotation={[Math.PI / 2, 0, 0]}
        ref={camRef}
        position={[0, -10, 0]}
        zoom={5}
      ></OrthographicCamera>

      {selected.map((s, i) => {
        return (
          <Suspense fallback={null}>
            <Quad
              data={results[s]}
              color={colorHash.hex(results[s].racer.name + s)}
            ></Quad>
          </Suspense>
        );
      })}
      <Bounds fit clip observe damping={6} margin={1.2}>
        <BoundBox></BoundBox>
      </Bounds>
    </>
  );
}
export default App;
function BoundBox() {
  //useHelper(camRef, CameraHelper, "red");
  let { bounds } = useStore();
  let b = useBounds();
  //bounds to center and size:
  let sizeX = bounds.maxX - bounds.minX;
  let sizeY = bounds.maxY - bounds.minY;
  let sizeZ = bounds.maxZ - bounds.minZ;
  let posX = (bounds.maxX + bounds.minX) / 2;
  let posY = (bounds.maxY + bounds.minY) / 2;
  let posZ = (bounds.maxZ + bounds.minZ) / 2;
  console.log(posX, posY, posZ, sizeX, sizeY, sizeZ);
  useEffect(() => {
    console.log({ b });
    b.refresh().clip().fit();
  }, [bounds, b]);
  return (
    <group position={[posX, posY, posZ]}>
      <boxBufferGeometry args={[sizeX, sizeY, sizeZ]}></boxBufferGeometry>
    </group>
  );
}
function makeRacers(data) {
  console.log(data);
  let racers = {};
  data.results.forEach((r) => {
    if (racers[r.racer.id]) {
      racers[r.racer.id].results.push({
        time: r.time,
        id: r.id,
        model: r.model,
        file: r.file[0] || null,
      });
    } else {
      racers[r.racer.id] = {
        ...r.racer,
        results: [
          {
            time: r.time,
            id: r.id,
            model: r.model,
            file: r.file[0] || null,
          },
        ],
      };
    }
  });
  racers = Object.keys(racers).map((k) => {
    let r = racers[k];
    return { ...r, results: r.results.sort((a, b) => a.time - b.time) };
  });
  let track = {
    name: data.name,
    racers: racers.sort((a, b) => {
      return a.results[0].time - b.results[0].time;
    }),
  };
  return track;
}

function Quad({ data, color }) {
  const ref = useRef();
  let clock = useStore((s) => s.clock);
  let setBounds = useStore((s) => s.setBounds);
  const ghost = suspend(async () => {
    const res = await fetch(
      `https://fastboiii.sigsegowl.xyz/file/${data.file[0].url}`
    );
    return res.json().then((d) => {
      console.log(d);
      let initTime = d.frames[0].time;
      return {
        ...d,
        frames: d.frames.map((f, i) => {
          return {
            ...f,
            key: i,
            time: f.time - initTime,
            position: new THREE.Vector3(f.x, f.y, f.z),
          };
        }),
      };
    });
  }, [data.id]);
  console.log(ghost);

  useEffect(() => {
    setBounds({
      minX: ghost.minX,
      minY: ghost.minY,
      minZ: ghost.minZ,
      maxX: ghost.maxX,
      maxY: ghost.maxY,
      maxZ: ghost.maxZ,
    });
  }, [ghost, setBounds]);

  useFrame(() => {
    //find first frame after elapsed time:

    let dtime = clock.getElapsedTime();
    if (dtime === 0) {
      return;
    }
    let secondFrame = ghost.frames.find((d) => d.time > dtime);
    if (secondFrame) {
      let firstFrame = ghost.frames[secondFrame.key - 1];
      let endTime = secondFrame.time - firstFrame.time;
      let currTime = dtime - firstFrame.time;
      let alpha = currTime / endTime;
      ref.current.position.copy(
        firstFrame.position.clone().lerp(secondFrame.position, alpha)
      );
    }
  });

  return (
    <>
      <Trail length={1200} width={4} color={"#666666"}>
        <mesh ref={ref} position={ghost.frames[0].position}>
          <boxBufferGeometry args={[1, 1, 1]}></boxBufferGeometry>
          <meshStandardMaterial color={color}></meshStandardMaterial>
          <Html
            style={{
              fontSize: "80%",
              background: "#00000044",
              padding: 4,
              borderRadius: 4,
            }}
          >
            {data.racer.name}
          </Html>
        </mesh>
      </Trail>
    </>
  );
}
